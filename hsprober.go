package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sycamoreone/orc/control"
	"golang.org/x/net/proxy"
)

var (
	httpAddr           = flag.String("http-addr", ":3131", "address for the HTTP server")
	torAddr            = flag.String("tor-addr", "localhost:9050", "address of the Tor SOCKS5 proxy")
	torControlAddr     = flag.String("tor-control-addr", "/var/run/tor/control", "Tor control address (tcp or unix)")
	targetAddrs        = flag.String("targets", "", "target HS addrs, comma separated")
	makeNewCircuits    = flag.Bool("new-circuits", false, "create a new Tor circuit every time")
	probeInterval      = flag.Duration("interval", 120*time.Second, "interval between probes")
	numProbesPerTarget = flag.Int("probes", 1, "number of sequential probes for each target")

	circuitSetupTimeout    = flag.Duration("circuit-setup-timeout", 10*time.Second, "timeout for initial circuit setup")
	firstConnectionTimeout = flag.Duration("first-connection-timeout", 30*time.Second, "timeout for the first (circuit warmup) connection")
	connectionTimeout      = flag.Duration("timeout", 10*time.Second, "timeout for each probe connection")
)

func makeBuckets(base, max, exp float64) []float64 {
	var buckets []float64
	for ; base < max; base *= exp {
		buckets = append(buckets, base)
	}
	return buckets
}

// Prometheus metric definition.
var (
	probeLatency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "tor_connection_latency",
			Help:    "Connection latency to target, over the Tor network.",
			Buckets: makeBuckets(0.02, 30, 1.4142),
		},
		[]string{"target"},
	)

	probeErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "tor_connection_errors",
			Help: "Number of connection errors per target and by type.",
		},
		[]string{"target", "error"},
	)

	probeCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "tor_connections",
			Help: "Number of connections by target.",
		},
		[]string{"target"},
	)
)

func init() {
	prometheus.MustRegister(probeLatency)
	prometheus.MustRegister(probeErrors)
	prometheus.MustRegister(probeCounter)
}

// Since golang.org/x/net/proxy does not have DialContext, create a Dialer that
// supports setting a timeout, so we don't wait forever for unreachable targets.
type timeoutDialer struct {
	*net.Dialer
	deadline time.Time
}

func newTimeoutDialer(ctx context.Context) *timeoutDialer {
	deadline, _ := ctx.Deadline()
	return &timeoutDialer{
		Dialer:   &net.Dialer{Deadline: deadline},
		deadline: deadline,
	}
}

func (d timeoutDialer) Dial(network, address string) (net.Conn, error) {
	conn, err := d.Dialer.Dial(network, address)
	if err != nil {
		return nil, err
	}
	if err = conn.SetDeadline(d.deadline); err != nil {
		conn.Close()
		return nil, err
	}
	return conn, err
}

// A simple tcp connection test: open a connection and immediately close it,
// without exchanging any traffic.
func checkConnection(ctx context.Context, target string) error {
	p, err := proxy.SOCKS5("tcp", *torAddr, nil, newTimeoutDialer(ctx))
	if err != nil {
		return err
	}

	conn, err := p.Dial("tcp", target)
	if err != nil {
		return err
	}
	return conn.Close()
}

func checkConnectionWithTimeout(ctx context.Context, target string, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()
	err := checkConnection(ctx, target)
	// If the context has timed out, return a DeadlineExceeded error instead
	// of the one returned by checkConnection (the proxy library masks
	// network errors with a fmt.Errorf call).
	if cerr := ctx.Err(); cerr != nil {
		return cerr
	}
	return err
}

func instrumentedCheckConnectionWithTimeout(ctx context.Context, target string, timeout time.Duration) error {
	start := time.Now()
	err := checkConnectionWithTimeout(ctx, target, timeout)
	elapsed := time.Since(start).Seconds()

	probeCounter.WithLabelValues(target).Inc()
	if err != nil {
		errType := "ERROR"
		if err == context.DeadlineExceeded {
			errType = "TIMEOUT"
		}
		probeErrors.WithLabelValues(target, errType).Inc()
	} else {
		probeLatency.WithLabelValues(target).Observe(elapsed)
	}

	return err
}

func checkConnectionWithTimeoutNTimes(ctx context.Context, target string, timeout time.Duration, numProbes int) {
	for i := 0; i < numProbes; i++ {
		if err := instrumentedCheckConnectionWithTimeout(ctx, target, timeout); err != nil {
			log.Printf("%s: error: %v\n", target, err)
		}
	}
}

func checkConnectionNTimesWithWarmup(ctx context.Context, target string, timeout time.Duration, numProbes int) {
	if *makeNewCircuits {
		// The first connection takes longer because we might need to wait for
		// the circuit to build and to discover the hidden service descriptor.
		//
		// This connection is instrumented, but not for latency (we know it's
		// going to be slow).
		probeCounter.WithLabelValues(target).Inc()
		if err := checkConnectionWithTimeout(ctx, target, *firstConnectionTimeout); err != nil {
			errType := "ERROR"
			if err == context.DeadlineExceeded {
				errType = "TIMEOUT"
			}
			probeErrors.WithLabelValues(target, errType).Inc()
		}
	}

	checkConnectionWithTimeoutNTimes(ctx, target, timeout, numProbes)
}

var (
	newnymDelay = 8 * time.Second
	splayTime   = 1 * time.Second
)

// Check multiple targets in parallel (each with numProbes sequential
// probes). Returns the check results, in random order.
func checkManyTargets(ctx context.Context, targets []string, timeout time.Duration, numProbes int) {
	var wg sync.WaitGroup
	for _, target := range targets {
		wg.Add(1)
		go func(target string) {
			// Stagger the first connection a bit.
			time.Sleep(time.Duration(rand.Int63n(int64(splayTime))))
			checkConnectionNTimesWithWarmup(ctx, target, timeout, numProbes)
			wg.Done()
		}(target)
	}

	wg.Wait()
}

// Fetch the authentication token for the control socket, either via environment
// variable, or by looking for the authcookie file in the socket directory.
func getTorAuth(addr string) string {
	if s := os.Getenv("TOR_CONTROL_PASSWORD"); s != "" {
		return s
	}
	if !strings.HasPrefix(addr, "/") {
		return ""
	}
	cookiePath := filepath.Join(filepath.Dir(addr), "control.authcookie")
	if _, err := os.Stat(cookiePath); os.IsNotExist(err) {
		return ""
	}
	data, err := ioutil.ReadFile(cookiePath)
	if err != nil {
		return ""
	}
	return string(data)
}

func parseValue(reply *control.Reply) string {
	if len(reply.Lines) < 1 {
		return ""
	}
	s := reply.Lines[0].Text
	parts := strings.SplitN(s, "=", 2)
	if len(parts) != 2 {
		return s
	}
	return parts[1]
}

type torControlConnection struct {
	*control.Conn
	netConn net.Conn
}

func (c *torControlConnection) Close() {
	_ = c.netConn.Close()
}

func newTorControlConnection(addr string) (*torControlConnection, error) {
	network := "tcp"
	if strings.HasPrefix(addr, "/") {
		network = "unix"
	}
	conn, err := net.Dial(network, addr)
	if err != nil {
		return nil, err
	}
	torc := control.Client(conn)

	if err = torc.Auth(getTorAuth(addr)); err != nil {
		return nil, err
	}

	return &torControlConnection{Conn: torc, netConn: conn}, nil
}

var torStatusChecks = []struct {
	key      string
	expected string
}{
	//{"status/circuit-established", "1"},
	{"status/enough-dir-info", "1"},
	{"network-liveness", "up"},
}

// Check that a number of Tor status variables have the expected state, to
// indicate that networking is ready and available.
func (c *torControlConnection) isNetworkUp() (bool, error) {
	for _, check := range torStatusChecks {
		reply, err := c.GetInfo(check.key)
		if err != nil {
			return false, fmt.Errorf("GetInfo(%s) failed: %v", check.key, err)
		}
		value := parseValue(reply)
		if value != check.expected {
			log.Printf("circuit not ready (%s: %s)\n", check.key, value)
			return false, nil
		}
	}
	return true, nil
}

// Wait until the Tor network layer is up, or the timeout expires.
func (c *torControlConnection) waitForNetwork(timeout time.Duration) error {
	deadline := time.Now().Add(timeout)
	for time.Now().Before(deadline) {
		ok, err := c.isNetworkUp()
		if err != nil {
			return err
		}
		if ok {
			log.Printf("circuit is ready")
			return nil
		}
		time.Sleep(time.Second)
	}
	return errors.New("deadline exceeded waiting for circuit")
}

// Send a NEWNYM signal to the Tor daemon.
func (c *torControlConnection) newCircuit() error {
	if err := c.Signal("NEWNYM"); err != nil {
		return err
	}

	// Give it time to build a new circuit?
	// Apparently Tor has a NEWNYM delay of 7 seconds.
	time.Sleep(newnymDelay)
	reply, err := c.GetInfo("circuit-status")
	if err != nil {
		return err
	}
	log.Printf("circuit-status:\n%s\n", reply.Lines[0].Data)
	return nil
}

func runLoop(targets []string, stop <-chan bool) error {
	ctrl, err := newTorControlConnection(*torControlAddr)
	if err != nil {
		return err
	}
	defer ctrl.Close()

	if *makeNewCircuits {
		if err := ctrl.newCircuit(); err != nil {
			return err
		}
	}
	if err := ctrl.waitForNetwork(*circuitSetupTimeout); err != nil {
		return err
	}

	tick := time.NewTicker(*probeInterval)
	defer tick.Stop()
	for {
		checkManyTargets(context.Background(), targets, *connectionTimeout, *numProbesPerTarget)

		select {
		case <-tick.C:
		case <-stop:
			return nil
		}
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	minInterval := newnymDelay + *circuitSetupTimeout + *firstConnectionTimeout + splayTime + time.Duration(*numProbesPerTarget)*(*connectionTimeout)
	if *probeInterval < minInterval {
		log.Fatalf("probe interval must be greater than %g seconds", minInterval.Seconds())
	}

	stop := make(chan bool)

	// Start an http.Server to export Prometheus metrics.
	h := http.NewServeMux()
	h.Handle("/metrics", promhttp.Handler())
	srv := &http.Server{
		Addr:    *httpAddr,
		Handler: h,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("error starting the HTTP server: %v", err)
			close(stop)
		}
	}()

	// Add a signal handler to gracefully stop the scheduler loop and exit.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received signal, exiting")
		close(stop)

		// Wait 3 seconds for connections to terminate.
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			log.Printf("error stopping HTTP server: %v", err)
			srv.Close()
		}
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	if err := runLoop(strings.Split(*targetAddrs, ","), stop); err != nil {
		log.Fatal(err)
	}

}
