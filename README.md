hsprober
===

Monitors availability of Tor Hidden Services using simple TCP probes,
and exports the results to Prometheus.

The tool periodically runs a set of probes towards one or more target
Hidden Services.  It can start a new Tor circuit on every cycle if
desired (using the *--new-circuits* command-line option), in which
case it will warm-up the circuit with a test probe before starting
measurements for the target. A *probe* is simply a TCP connection
attempt: the prober won't exchange any data on the connection, a probe
is considered successful if the connection is established.

By default, it will run one probe per service every probing
interval. If you are interested in measuring connection latency, it
can be useful to increase this number with the *--probes* command-line
option, to run multiple connection probes for each target one after
another in sequence. This gives better statistical accuracy for
latency measurements at a specific point in time. Note that, if using
*--new-circuits*, the latency of the first connection is ignored (as
it presumably includes the time to fetch the HS descriptor).

## Installation

You will need a working [Go](https://golang.org/) environment. Then,
simply running:

    $ go get git.autistici.org/ale/hsprober

should build and install the *hsprober* binary in `$GOPATH/bin`.

## Usage

The *hsprober* binary is a standalone daemon. Run it with *--help* to
get the full list of supported options. The most important one is
*--targets*, which should specify a list of *host:port* Hidden Service
targets to be probed, separated by commas, e.g.:

    $ hsprober --targets=autinv5q6en4gpf4.onion:443

will monitor the *autinv5q6en4gpf4.onion* Hidden Service on port 443.

The tool needs access to the Tor control socket in order to set up
circuits (it will also use it to wait until Tor reports that it can
successfully access the network on startup). Consider using a
dedicated Tor instance for hsprober, so that its circuit switching
does not affect other traffic.

## Metrics

Metrics are collected and exported in Prometheus-compatible format
(under the default `/metrics` path) by a small embedded HTTP server.

The following metrics are collected, each with a *target* label:

* `tor_connection_latency` is a histogram (with *exp(sqrt(2))*
  buckets) of connection latencies
* `tor_connections` is a counter of the total number of attempted
  connections
* `tor_connection_errors` counts connection errors: it has an
  additional label *error* which identifies the error type (either
  TIMEOUT for timeouts, or ERROR for any other error).
